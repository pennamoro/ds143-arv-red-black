#include <stdio.h>

struct arvore {
    int info;
    int cor;
    struct arvore* pai;
    struct arvore* dir;
    struct arvore* esq;
};

struct arvore* raiz = NULL;

struct arvore* insert(struct arvore* arv, struct arvore* temp){
    if (arv == NULL)
        return temp;

    if (temp->info < arv->info)
    {
        arv->esq = insert(arv->esq, temp);
        arv->esq->pai = arv;
    }
    else if (temp->info > arv->info)
    {
        arv->dir = insert(arv->dir, temp);
        arv->dir->pai = arv;
    }
    return arv;
}

void rodadir(struct arvore* temp){
    struct arvore* esq = temp->esq;
    temp->esq = esq->dir;

    if (temp->esq){temp->esq->pai = temp;}
    esq->pai = temp->pai;

    if (!temp->pai){
        raiz = esq;
    }else if (temp == temp->pai->esq){
        temp->pai->esq = esq;
    }else{
        temp->pai->dir = esq;
    }
    esq->dir = temp;
    temp->pai = esq;
}

void rodaesq(struct arvore* temp){
    struct arvore* dir = temp->dir;
    temp->dir = dir->esq;

    if (temp->dir){temp->dir->pai = temp;}
    dir->pai = temp->pai;

    if (!temp->pai){
        raiz = dir;
    }else if (temp == temp->pai->esq){
        temp->pai->esq = dir;
    }else{
        temp->pai->dir = dir;
    }
    dir->esq = temp;
    temp->pai = dir;
}

void ajuste(struct arvore* raiz, struct arvore* arv){
    struct arvore* pai = NULL;
    struct arvore* avo = NULL;

    while ((arv != raiz) && (arv->cor != 0)&& (arv->pai->cor == 1)){
        pai = arv->pai;
        avo = arv->pai->pai;
        //Pai de arv � filho da esquerda do avo dae arv
        if (pai == avo->esq){
            struct arvore* tio = avo->dir;
            //Tio de arv � vermelho: somente troca cor
            if (tio != NULL && tio->cor == 1){
                avo->cor = 1;
                pai->cor = 0;
                tio->cor = 0;
                arv = avo;
            //arv � a filha da direita: rota��o � esquerda
            }else {
                if (arv == pai->dir) {
                    rodaesq(pai);
                    arv = pai;
                    pai = arv->pai;
                }
            //arv � a filha da esquerda: rota��o � direita
                rodadir(avo);
                int t = pai->cor;
                pai->cor = avo->cor;
                avo->cor = t;
                arv = pai;
            }
        }
        //Pai de arv � filho da direita do avo de arv
        else{
            struct arvore* tio = avo->esq;
            //Tio de arv � vermelho: somente troca cor
            if((tio != NULL) && (tio->cor == 1)){
                avo->cor = 1;
                pai->cor = 0;
                tio->cor = 0;
                arv = avo;
            }else {
            //arv � a filha da esquerda: rota��o � direita
                if(arv == pai->esq){
                    rodadir(pai);
                    arv = pai;
                    pai = arv->pai;
                }
            //arv � a filha da direita: rota��o � esquerda
                rodaesq(avo);
                int t = pai->cor;
                pai->cor = avo->cor;
                avo->cor = t;
                arv = pai;
            }
        }
    }
    raiz->cor = 0;
}

void main(){
 //realizar a inser��o primeiro e depois o ajuste para manter a organiza��o da �rvore
}
